from django.db import models
from django.contrib.auth.models import User

class Location(models.Model):
    longitude   = models.FloatField()
    latitude    = models.FloatField()
    address     = models.TextField()

class Post(models.Model):
    user        = models.ForeignKey(User)
    location    = models.ForeignKey(Location, null = True)
    text        = models.TextField()
    date        = models.DateTimeField()
    likes       = models.ManyToManyField(User, related_name = "liked")
