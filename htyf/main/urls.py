from django.conf.urls import patterns, include, url

from main.views import *

urlpatterns = patterns('',
    url(r'^$', main, name="main"),
    url(r'^history', history, name="history"),
    url(r'^login/', login, name="login"),
    url(r'^logout/', logout, name="logout"),
    url(r'^post/', post, name="post"),
    url(r'^shortestpath/', shortestpath, name="shortestpath"),
    url(r'^like/(?P<post_id>\d+)/', like, name="like"),
    url(r'^unlike/(?P<post_id>\d+)/', unlike, name="unlike"),
    url(r'^delete_post/(?P<post_id>\d+)/', delete_post, name="delete_post"),
    url(r'^path/(?P<start_id>\d+)/', bestpath, name="path"),
    url(r'^locations/', locations, name="locations")
    )
