import math
import requests

from main.models    import *

GOOGLE_DIST = "https://maps.googleapis.com/maps/api/distancematrix/json"

def shortest(start, heuristic):
    locations = Location.objects.all()
    
    unvisited_nodes = []
    for location in locations:
        if not (location ==  start):
            unvisited_nodes.append({
                "longitude": location.longitude, 
                "latitude": location.latitude,
                "address": location.address
            })
    
    first_node = {
        "longitude": start.longitude, 
        "latitude": start.latitude,
        "address": start.address
    }
    visited_nodes = [first_node]
    last_node = first_node
    while(unvisited_nodes):
        next_node = unvisited_nodes.pop(closest_node(last_node, unvisited_nodes, heuristic))
        visited_nodes.append(next_node)
        last_node = next_node
        
    return visited_nodes
    
def closest_node(current, unvisited_nodes, heuristic):
    closest = 0
    closest_dist = distance(current, unvisited_nodes[0], heuristic)
    
    for i in range(1, len(unvisited_nodes)):
        dist = distance(current, unvisited_nodes[i], heuristic)
        if (dist < closest_dist):
            closest = i
            closest_dist = dist
            
    return closest
    
def distance(location1, location2, heuristic):
    if (heuristic == "manhattan"):
        return math.fabs(location1["longitude"] - location2["longitude"]) \
            + math.fabs(location1["latitude"] - location2["latitude"])
            
    elif (heuristic == "euclidian"):
        return math.hypot(math.fabs(location1["longitude"] - location2["longitude"]),
            math.fabs(location1["latitude"] - location2["latitude"]))
            
    elif (heuristic == "road"):
        params = {
            "origins": "{latitude},{longitude}".format(**location1),
            "destinations": "{latitude},{longitude}".format(**location2)
        }
        req = requests.get(GOOGLE_DIST, params = params)
        json = req.json()
        distance = json["rows"][0]["elements"][0]["distance"]["value"] #in metres
        return distance
    
    else:
        return None
