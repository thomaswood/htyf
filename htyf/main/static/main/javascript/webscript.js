/*This is javascript code which has been used 
in the website

Some of the functions used in Script


getLocation()
showPosition()
showError()
convertAdd()
saveAdd()
showMapHistory()
initialize()
showMeMap()
showMeUpdates()


*/

var check=false;                //used for adding toggle facility in history webpage 


//=====here are the functions related to the maps==================

var x = document.getElementById("errorMess");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

//Function which loads the position of the User

function showPosition(position) {
    lat = position.coords.latitude;
    lon = position.coords.longitude;
	convertAdd(lat,lon);
    latlon = new google.maps.LatLng(lat, lon)
    mapholder = document.getElementById('mapload')
    mapholder.style.height = '250px';
    mapholder.style.width = '760px';

    var myOptions = {
    center:latlon,zoom:14,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    mapTypeControl:false,
    navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    }
    
    var map = new google.maps.Map(document.getElementById("mapload"), myOptions);
    var marker = new google.maps.Marker({position:latlon,map:map,title:"You are here!"});
}

//Function which shows the error on the webpage if geolocation is not allowed

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}




//this is a function which has been used to convert the lat and lon into some human readable address
function convertAdd(lat,lon)
{
//creating a new http request to get the data from the google


var formhandler=document.getElementById("myForm");
//creating an element to it (for latitude) 
var input = document.createElement("input");
//setting attributes and value
input.setAttribute("type","hidden");
input.setAttribute("value",lat);	
input.setAttribute("id","lat");
input.setAttribute("name","latitude");

//adding this element to myForm
formhandler.appendChild(input);


//creating an element to it (for longitude) 
var input = document.createElement("input");
//setting attributes and value
input.setAttribute("type","hidden");
input.setAttribute("value",lon);	
input.setAttribute("id","lon");
input.setAttribute("name","longitude");


//adding this element to myForm
formhandler.appendChild(input);




var xmlhttp = new XMLHttpRequest();
var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lon;

xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        var webData = JSON.parse(xmlhttp.responseText);
        saveAdd(webData);//this function can save the json data and extract the address from it
		


    }
}
xmlhttp.open("GET", url, true);
xmlhttp.send();

	
}

//Function which is related to some Address functionality
    
function saveAdd(content)
{
	var address=content.results[0];
	
	x=document.getElementById("outputText");
	var finalAddress=address.formatted_address;
	x.innerHTML=finalAddress;
	//now we will can put this address in the database
	
	var formhandler=document.getElementById("myForm");
	
	//creating an element to it (for saving address) 
	var input = document.createElement("input");
	//setting attributes and value
	input.setAttribute("type","hidden");
	input.setAttribute("value",finalAddress);
	input.setAttribute("name","address");	
	
	//adding this element to myForm
	formhandler.appendChild(input);
	
	
	
	
}

/*
var LocationData = [
    [49.2812668, -123.1035942, "26 E Hastings St, Vancouver" ], 
    [49.2814064, -123.1025187, "71 E Hastings St, Vancouver" ], 
    [49.2812336, -123.1020622, "122 E Hastings St, Vancouver" ], 
    [49.2813564, -123.1012253, "138 E Hastings St, Vancouver" ], 
    [49.2811625, -123.0985032, "242 E Hastings St, Vancouver" ]
];
*/

//function which gets the info from the database about the places visited by user
function loadLocations()
{
	var xmlhttp = new XMLHttpRequest();
	var url = "http://localhost:8000/locations/";

	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	        var webData = JSON.parse(xmlhttp.responseText);
	        initialize(webData);//this function can save the json data and extract the address from it



	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}


//Function which is used for loading the markers with all the places visited by a User
 
function initialize(dataArr)
{
	var LocationData=dataArr;
	//getting the data from the file on the server

    var map = new google.maps.Map(document.getElementById('mapLoad'));
    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();
     
    for (var i in LocationData)
    {
        var p = LocationData[i];
        var latlng = new google.maps.LatLng(p.latitude, p.longitude);
        bounds.extend(latlng);
        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/'; //this is a location where the images are stored
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: p.address
        });
     
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent(this.title);
            infowindow.open(map, this);
        });
    }
     
    map.fitBounds(bounds);
}
 
google.maps.event.addDomListener(window, 'load', loadLocations);



//function which just shows the maps
function showMeMap()
{
	x=document.getElementById("feedwrapper");
	x.style.display="none";
	
	
}
//function which just shows the updates
function showMeUpdates()
{
	x=document.getElementById("feedwrapper");
	y=document.getElementById("mapLoad");
	if(check==false){
	x.style.display="block";
   	y.style.display="none";
	check=true;
	}
	else if(check==true)
	{
		x.style.display="none";
		y.style.display="block";
		check=false;
	}

}
//Function which is used to show the path on the map based upon the two addresses
 var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;

function initialize() {
  directionsDisplay = new google.maps.DirectionsRenderer();
  var chicago = new google.maps.LatLng(41.850033, -87.6500523);
  var mapOptions = {
    zoom:7,
    center: chicago
  };
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
  directionsDisplay.setMap(map);
}

function calcRoute() {
  var start = document.getElementById('start').value;
  var end = document.getElementById('end').value;
  var request = {
      origin:start,
      destination:end,
      travelMode: google.maps.TravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(response);
    }
  });
}

	

//====================some of the jquery functions==================

//this has been used for fading out the box, when we delete the update

$(document).ready(function(){
    $(".deleteme").click(function(){
        $(this).parent().fadeOut("slow");
        
        post_id = $(this).attr("post-id")
        xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET", "/delete_post/" + post_id, true);
        xmlhttp.send();
    });
});




