import json
from django.contrib                 import auth
from django.shortcuts               import render, redirect
from django.contrib.auth.decorators import login_required
from django.http                    import HttpResponse
from django.contrib.auth.models     import User
from datetime                       import datetime

from main.models                    import *
from main                           import path

def login(request):
    if ('username' in request.POST and 'password' in request.POST):  
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username = username, password = password)
        print user
    else:
        user = None
    
    if (user is None):
        return render(request, "main/login.html")
    else:
        auth.login(request, user)
        return redirect("main:main")

@login_required
def main(request):
    allposts = Post.objects.all()
    
    context = {
        "allposts" : allposts
    }
    return render(request, "main/main.html", context)
    
@login_required
def history(request):
    allposts = Post.objects.all()
    
    context = {
        "allposts" : allposts
    }
    return render(request, "main/history.html", context)
    
@login_required
def shortestpath(request):
    alllocations = Location.objects.all()
    
    if (request.method == "POST"):
        start = Location.objects.get(id = request.POST["start"])
        shortpath = path.shortest(start, "road")
    else:
        shortpath = None
    
    context = {
        "alllocations" : alllocations,
        "path": shortpath
    }
    return render(request, "main/shortestpath.html", context)
    
@login_required
def logout(request):
    auth.logout(request)
    return redirect("main:main")
    
@login_required
def post(request):
    if (request.method == "POST" and request.POST["text"]):
        if ("longitude" in request.POST and request.POST["longitude"]
        and "latitude" in request.POST and  request.POST["latitude"]):
            location = Location(longitude = request.POST["longitude"], latitude = request.POST["latitude"])
            
            if ("address" in request.POST):
                location.address = request.POST["address"]
            location.save()
        else:
            location = None
            
        post = Post(user = request.user, location = location, text = request.POST['text'], date = datetime.now())
        post.save()

    return redirect("main:main")
    
@login_required
def like(request, post_id):
    post = Post.objects.get(id = post_id)
    post.likes.add(request.user)
    print post.likes
    return redirect("main:main")
    
@login_required
def unlike(request, post_id):
    post = Post.objects.get(id = post_id)
    post.likes.remove(request.user)
    return redirect("main:main")
    
@login_required
def delete_post(request, post_id):
    post = Post.objects.get(id = post_id)
    if (post.user == request.user):
        post.location.delete()
        post.delete()
    
    return redirect("main:main")
    
def bestpath(request, start_id):
    start = Location.objects.get(id = start_id)
    shortpath = path.shortest(start, "road")
    return HttpResponse(json.dumps(shortpath), content_type="application/json")
    
@login_required
def locations(request):
    posts = Post.objects.filter(user = request.user)
    
    locations = []
    for post in posts:
        locations.append({
            "longitude": post.location.longitude, 
            "latitude": post.location.latitude, 
            "address": post.location.address
        })
        
    return HttpResponse(json.dumps(locations), content_type="application/json")
